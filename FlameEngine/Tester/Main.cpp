#include <iostream>
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>

#include "FlameEngine.h"

int main(int argc, char *argv[])
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	flame::extensions::Logger::ClearLog();
	flame::Flame engine;
	engine.Init(3, 3);
	engine.AttachWindow(new flame::Window("Flame Engine", 640, 480, false));
	flame::graphics::ShaderManager shaderManager;
	flame::graphics::ShaderProgram *program = shaderManager.GetShader("Test.shader");

	std::vector<glm::vec4> d;
	d.push_back(glm::vec4(-1, -1, 0, 1));
	d.push_back(glm::vec4(0, 1, 0, 1));
	d.push_back(glm::vec4(1, -1, 0, 1));
	flame::graphics::Mesh mesh;
	mesh.SetVertices(d);
	mesh.Send();

	//------Object------
	flame::graphics::MeshRenderer *renderer = new flame::graphics::MeshRenderer();
	renderer->AttachMesh(&mesh);

	flame::graphics::Material material;
	material.AttachShader(program);
	material.SetVec3("colour", glm::vec3(0.21f, 0.31f, 0.49f));

	flame::Object *object = new flame::Object();
	object->transform.SetGlobalPosition(glm::vec3(0.5f, 0.f, 0.f));
	renderer->material = &material;
	object->renderComponents.push_back(renderer);
	engine.AddObject(object);
	//------------------

	//-----Camera-------
	flame::PerspectiveCamera *perspectiveCamera = new flame::PerspectiveCamera();
	engine.AddObject(perspectiveCamera);
	perspectiveCamera->transform.SetGlobalPosition(glm::vec3(0,0,10));
	//------------------

	engine.Work();
	engine.Destroy();

	shaderManager.DeleteShaders();

	return 0;
}