version 400 core
<vertex>
layout(location = 0) in vec4 vertex;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

void main()
{
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vertex;
}
</vertex>
<fragment>
uniform vec3 colour;

out vec4 fragmentColour;

void main()
{
	fragmentColour = vec4(colour, 1.0);
}
</fragment>