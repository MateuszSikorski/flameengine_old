#include "ParsedVariable.h"

namespace flame
{
	namespace graphics
	{
		ParsedVariableType ParseType(std::string data)
		{
			if (data == "int")
				return ParsedVariableType::INT;
			else if (data == "float")
				return ParsedVariableType::FLOAT;
			else if (data == "vec2")
				return ParsedVariableType::VEC2;
			else if (data == "vec3")
				return ParsedVariableType::VEC3;
			else if (data == "vec4")
				return ParsedVariableType::VEC4;
			else if (data == "mat4")
				return ParsedVariableType::MAT4;
			else
				return ParsedVariableType::UNKNOW_TYPE;
		}
	}
}