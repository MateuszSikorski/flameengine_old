#pragma once

#define GLM_ENABLE_EXPERIMENTAL

#include <string>
#include <sstream>
#include <vector>
#include <map>

#include <glew.h>
#include <glfw3.h>
#include <glm.hpp>
#include <gtc\quaternion.hpp>
#include <gtx\quaternion.hpp>
#include <gtc\matrix_transform.hpp>
#include <gtx\transform.hpp>