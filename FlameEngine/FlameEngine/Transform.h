#pragma once

#include "HeaderFiles.h"

namespace flame
{
	struct TransformSpaceData
	{
		glm::vec3 position;
		glm::quat rotation;
		glm::vec3 scale;

		TransformSpaceData(glm::vec3 pos = glm::vec3(0, 0, 0), glm::quat rot = glm::quat(1, 0, 0, 0),
			glm::vec3 scale = glm::vec3(1, 1, 1));

		glm::mat4 GetTranslation();
	};

	class Transform
	{
		friend class Transform;
	private:
		TransformSpaceData global;
		TransformSpaceData local;
		Transform *parent;
		std::vector<Transform*> children;

		void RefreshChildren();
		void RefreshGlobal();

	public:
		Transform();
		virtual ~Transform();

		void SetGlobalPosition(glm::vec3 pos);
		void SetGlobalRotation(glm::quat rot);
		void SetGlobalEulerAngles(glm::vec3 rot);
		void SetGlobalScale(glm::vec3 s);

		void SetLocalPosition(glm::vec3 pos);
		void SetLocalRotation(glm::quat rot);
		void SetLocalEulerAngles(glm::vec3 rot);
		void SetLocalScale(glm::vec3 s);

		glm::vec3 GetGlobalPosition() { return global.position; }
		glm::quat GetGlobalRotation() { return global.rotation; }
		glm::vec3 GetGlobalEulerAngles();
		glm::vec3 GetGlobalScale() { return global.scale; }

		glm::vec3 GetLocalPosition() { return local.position; }
		glm::quat GetLocalRotation() { return local.rotation; }
		glm::vec3 GetLocalScale() { return local.scale; }

		glm::mat4 GetModelMatrix();

		void RemoveParent();
		void AddChild(Transform *child);
		void RemoveChild(int num);
		void RemoveChild(Transform *child);

		int GetChildrenCount() { return children.size(); }
		Transform *GetParent() { return parent; }
		Transform *GetChild(int num);
	};
}