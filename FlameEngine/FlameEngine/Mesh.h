#pragma once
#include "GraphicsBuffer.h"

namespace flame
{
	namespace graphics
	{
		class Mesh
		{
		protected:
			GLuint vao;
			GraphicsBuffer<glm::vec4> vertices;

		public:
			Mesh();
			virtual ~Mesh();

			void SetVertices(std::vector<glm::vec4> &vertices);
			void Send();
			void Delete();
			void Bind();

			std::vector<glm::vec4> GetVertices();
		};
	}
}