#pragma once
#include "HeaderFiles.h"

namespace flame
{
	namespace graphics
	{
		template<typename T> class GraphicsBuffer
		{
		protected:
			GLuint vbo;
			GLenum type;
			GLenum usageType;
			std::vector<T> data;

		public:
			GraphicsBuffer();
			GraphicsBuffer(GLenum type, GLenum usageType);
			virtual ~GraphicsBuffer();

			void SetData(std::vector<T> &newData);
			void SetDataPointer(int index, int size, GLenum type, GLboolean normalize);
			void SendData();

			std::vector<T> GetData();
		};

		template class GraphicsBuffer<int>;
		template class GraphicsBuffer<float>;
		template class GraphicsBuffer<glm::vec2>;
		template class GraphicsBuffer<glm::vec3>;
		template class GraphicsBuffer<glm::vec4>;
	}
}