#pragma once
#include "RenderComponent.h"
#include "Mesh.h"
#include "PerspectiveCamera.h"

namespace flame
{
	namespace graphics
	{
		class MasterRenderer
		{
		private:
			ShaderProgram *activeShader;
			Material *activeMaterial;
			std::vector<Camera*> cameras;

			void ActivateShader();
			void ActivateMaterial(RenderComponent *renderer);

		public:
			MasterRenderer();
			virtual ~MasterRenderer();

			void AddCamera(Object *object, glm::vec2 viewPort);
			void Render(std::vector<Object*> objects);
			void ClearCameraList();
		};
	}
}