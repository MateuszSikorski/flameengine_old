#pragma once

#include "ShaderVariables.h"

namespace flame
{
	namespace graphics
	{
		class MaterialVariable
		{
		protected:
			ShaderVariable *variable;

		public:
			MaterialVariable()
			{
				variable = nullptr;
			}

			virtual void SendValue() = 0;
			virtual void SetVariable(ShaderVariable *newVariable) = 0;

			std::string GetName()
			{
				return variable->name;
			}
		};

		class MaterialVariableInt : public MaterialVariable
		{
		public:
			int value;

			virtual void SetVariable(ShaderVariable *newVariable) override
			{
				variable = dynamic_cast<ShaderVariableInt*>(newVariable);
			}

			virtual void SendValue() override
			{
				dynamic_cast<ShaderVariableInt*>(variable)->value = value;
			}
		};

		class MaterialVariableFloat : public MaterialVariable
		{
		public:
			float value;

			virtual void SetVariable(ShaderVariable *newVariable) override
			{
				variable = dynamic_cast<ShaderVariableFloat*>(newVariable);
			}

			virtual void SendValue() override
			{
				dynamic_cast<ShaderVariableFloat*>(variable)->value = value;
			}
		};

		class MaterialVariableVec2 : public MaterialVariable
		{
		public:
			glm::vec2 value;

			virtual void SetVariable(ShaderVariable *newVariable) override
			{
				variable = dynamic_cast<ShaderVariableVec2*>(newVariable);
			}

			virtual void SendValue() override
			{
				dynamic_cast<ShaderVariableVec2*>(variable)->value = value;
			}
		};

		class MaterialVariableVec3 : public MaterialVariable
		{
		public:
			glm::vec3 value;

			virtual void SetVariable(ShaderVariable *newVariable) override
			{
				variable = dynamic_cast<ShaderVariableVec3*>(newVariable);
			}

			virtual void SendValue() override
			{
				dynamic_cast<ShaderVariableVec3*>(variable)->value = value;
			}
		};

		class MaterialVariableVec4 : public MaterialVariable
		{
		public:
			glm::vec4 value;

			virtual void SetVariable(ShaderVariable *newVariable) override
			{
				variable = dynamic_cast<ShaderVariableVec4*>(newVariable);
			}

			virtual void SendValue() override
			{
				dynamic_cast<ShaderVariableVec4*>(variable)->value = value;
			}
		};
	}
}