#pragma once
#include "Camera.h"

namespace flame
{
	class PerspectiveCamera : public Camera
	{
	protected:
		float aspect;
		float fov;

	public:
		PerspectiveCamera();
		virtual ~PerspectiveCamera();

		virtual void SetViewport(glm::ivec2 viewport) override;
		virtual void SetAspect();
		virtual void SetFOV(float value);
		virtual void SetNearPlane(float value) override;
		virtual void SetFarPlane(float value) override;
		virtual void RefreshSceneForRendering() override;
		virtual glm::mat4 GetViewMatrix() override;
		virtual glm::mat4 GetProjectionMatrix() override;

		float GetAspect() { return aspect; }
		float GetFOV() { return fov; }
	};
}