#pragma once
#include "HeaderFiles.h"

namespace flame
{
	class GPUdata
	{
	public:
		std::string vendor;
		std::string renderer;
		std::string openGLVersion;
		std::string glslVersion;

		GPUdata();
		virtual ~GPUdata();

		void GetDataFromDevice();
	};
}

