#pragma once

#include "HeaderFiles.h"
#include "ParsedVariable.h"

namespace flame
{
	namespace graphics
	{
		struct ParsedShader
		{
			std::string vertexShaderCode;
			std::string fragmentShaderCode;
			std::vector <ParsedVariable> variables;
		};
	}
}