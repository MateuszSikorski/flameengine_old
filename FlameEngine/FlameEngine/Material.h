#pragma once
#include "ShaderProgram.h"
#include "MaterialVariables.h"

namespace flame
{
	namespace graphics
	{
		class Material
		{
		protected:
			ShaderProgram * shader;
			std::vector<MaterialVariable*> variables;

			void InitVariables();
			MaterialVariable *GetVariableByName(std::string name);

		public:
			Material();
			virtual ~Material();

			void AttachShader(ShaderProgram *newShader);
			void SendValues();
			void Destroy();

			void SetInt(std::string name, int value);
			void SetFloat(std::string name, float value);
			void SetVec2(std::string name, glm::vec2 value);
			void SetVec3(std::string name, glm::vec3 value);
			void SetVec4(std::string name, glm::vec4 &value);

			ShaderProgram *GetShader();
		};
	}
}