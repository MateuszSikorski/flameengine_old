#pragma once

#include "HeaderFiles.h"

namespace flame
{
	namespace graphics
	{
		enum class ParsedVariableType
		{
			INT,
			FLOAT,
			VEC2,
			VEC3,
			VEC4,
			MAT4,
			UNKNOW_TYPE
		};

		ParsedVariableType ParseType(std::string data);

		struct ParsedVariable
		{
			std::string name;
			ParsedVariableType type;
		};
	}
}