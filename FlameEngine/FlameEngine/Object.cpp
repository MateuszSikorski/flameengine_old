#include "Object.h"

namespace flame
{
	Object::Object()
	{
	}

	Object::~Object()
	{
		DestroyComponents();
	}

	void Object::DestroyComponents()
	{
		for (auto *renderer : renderComponents)
		{
			delete renderer;
		}

		renderComponents.clear();
	}
}