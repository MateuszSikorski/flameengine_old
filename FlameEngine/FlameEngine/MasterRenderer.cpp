#include "MasterRenderer.h"
#include <iostream>

namespace flame
{
	namespace graphics
	{
		MasterRenderer::MasterRenderer()
		{
			activeShader = nullptr;
			activeMaterial = nullptr;
		}

		MasterRenderer::~MasterRenderer()
		{
			ClearCameraList();
		}

		void MasterRenderer::AddCamera(Object *object, glm::vec2 viewPort)
		{
			Camera *newCamera = dynamic_cast<Camera*>(object);
			if (newCamera != nullptr)
			{
				newCamera->SetViewport(viewPort);
				cameras.push_back(newCamera);
			}

			PerspectiveCamera *newPerspectiveCamera = dynamic_cast<PerspectiveCamera*>(newCamera);
			if (newPerspectiveCamera != nullptr)
			{
				newPerspectiveCamera->SetAspect();
			}
		}

		void MasterRenderer::Render(std::vector<Object*> objects)
		{
			for (auto *camera : cameras)
			{
				camera->RefreshSceneForRendering();
				for (auto *object : objects)
				{
					for (auto *renderer : object->renderComponents)
					{
						if (renderer != nullptr)
						{
							ActivateMaterial(renderer);
							if (activeMaterial != nullptr && activeShader != nullptr)
							{
								activeShader->variableManager.SendProjectionMatrix(camera->GetProjectionMatrix());
								activeShader->variableManager.SendViewMatrix(camera->GetViewMatrix());
								activeShader->variableManager.SendModelMatrix(object->transform.GetModelMatrix());
								renderer->Render();
							}
						}
					}
				}
			}
		}

		void MasterRenderer::ClearCameraList()
		{
			cameras.clear();
		}

		void MasterRenderer::ActivateShader()
		{
			if (activeMaterial != nullptr)
			{
				ShaderProgram *program = activeMaterial->GetShader();
				if (program != nullptr)
				{
					if (activeShader != program)
					{
						activeShader = program;
						program->Use();
					}
					program->variableManager.Send();
				}
			}
		}

		void MasterRenderer::ActivateMaterial(RenderComponent *renderer)
		{
			if (activeMaterial != renderer->material)
			{
				if (renderer->material != nullptr)
				{
					activeMaterial = renderer->material;
					activeMaterial->SendValues();
					ActivateShader();
				}
			}
		}
	}
}