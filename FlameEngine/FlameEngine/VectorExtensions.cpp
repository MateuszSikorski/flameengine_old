#include "VectorExtensions.h"

using namespace std;

namespace flame
{
	namespace extensions
	{
		std::string ToString(glm::vec2 value)
		{
			string stringVector = to_string(value.x);
			stringVector += " " + to_string(value.y);

			return stringVector;
		}

		string ToString(glm::vec3 value)
		{
			string stringVector = to_string(value.x);
			stringVector += " " + to_string(value.y);
			stringVector += " " + to_string(value.z);

			return stringVector;
		}

		std::string ToString(glm::vec4 &value)
		{
			string stringVector = to_string(value.x);
			stringVector += " " + to_string(value.y);
			stringVector += " " + to_string(value.z);
			stringVector += " " + to_string(value.w);

			return stringVector;
		}
	}
}