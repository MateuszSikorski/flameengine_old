#include "ShaderCompiler.h"

namespace flame
{
	namespace graphics
	{
		ShaderCompiler::ShaderCompiler()
		{
		}

		ShaderCompiler::~ShaderCompiler()
		{
		}

		std::string ShaderCompiler::CreateErrorLogMessage()
		{
			std::string errorMessage;
			for (int i = 0; i < errorLog.size(); i++)
			{
				errorMessage += errorLog[i] + "\n";
			}

			return errorMessage;
		}

		void ShaderCompiler::CompileShader(std::string data, GLuint *shader, GLenum type, std::string logTag)
		{
			*shader = glCreateShader(type);
			const char *source = data.c_str();
			glShaderSource(*shader, 1, &source, nullptr);
			glCompileShader(*shader);

			int logLength = 0;
			GLchar log[1024];
			glGetShaderInfoLog(*shader, 1024, &logLength, log);

			if (logLength > 0)
			{
				std::string logMessage = logTag + log;
				errorLog.push_back(logMessage);
			}
		}

		void ShaderCompiler::LinkProgram(ShaderProgram *program)
		{
			program->programID = glCreateProgram();
			glAttachShader(program->programID, vertexShader);
			glAttachShader(program->programID, fragmentShader);
			
			glLinkProgram(program->programID);

			glDetachShader(program->programID, vertexShader);
			glDetachShader(program->programID, fragmentShader);

			int status;
			glGetProgramiv(program->programID, GL_COMPILE_STATUS, &status);
			if (status == GL_FALSE)
			{
				GLchar log[1024];
				glGetProgramInfoLog(program->programID, 1024, 0, log);
				std::string logMessage = log;
				errorLog.push_back(logMessage);
			}
		}

		ShaderProgram* ShaderCompiler::Compile(ParsedShader *parsedShader)
		{
			CompileShader(parsedShader->vertexShaderCode, &vertexShader, GL_VERTEX_SHADER, "VERTEX SHADER: ");
			CompileShader(parsedShader->fragmentShaderCode, &fragmentShader, GL_FRAGMENT_SHADER, "FRAGMENT SHADER: ");
			ShaderProgram *program = new ShaderProgram();
			LinkProgram(program);
			DeleteShaders();

			if (errorLog.size() > 0)
			{
				delete program;
				ShaderCompilerError error;
				error.message = CreateErrorLogMessage();
				throw error;
			}

			return program;
		}

		void ShaderCompiler::DeleteShaders()
		{
			glDeleteShader(vertexShader);
			glDeleteShader(fragmentShader);
		}
	}
}