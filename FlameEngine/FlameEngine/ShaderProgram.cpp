#include "ShaderProgram.h"

namespace flame
{
	namespace graphics
	{
		ShaderProgram::ShaderProgram()
		{
			programID = glCreateProgram();
		}

		ShaderProgram::~ShaderProgram()
		{
			glDeleteProgram(programID);
		}

		void ShaderProgram::Use()
		{
			glUseProgram(programID);
		}

		void ShaderProgram::InitVariables()
		{
			variableManager.GetVariablesLoactions(programID);
		}
	}
}