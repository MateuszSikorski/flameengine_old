#include "Logger.h"

namespace flame
{
	namespace extensions
	{
		Logger::Logger()
		{
		}

		Logger::~Logger()
		{
		}

		void Logger::ClearLog()
		{
			std::ofstream file("log.txt");
			file.clear();
			file.close();
		}

		void Logger::Log(std::string message)
		{
			std::ofstream file("log.txt", std::ios::app | std::ios::ate);
			message += "\n";
			file.write(message.c_str(), message.size());
			file.close();
		}
	}
}