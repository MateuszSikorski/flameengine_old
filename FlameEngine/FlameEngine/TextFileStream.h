#pragma once

#include "HeaderFiles.h"
#include <fstream>

namespace flame
{
	namespace extensions
	{
		enum class TextFileStreamErrorType
		{
			NO_LOAD_FILE,
			NO_SAVE_FILE
		};

		struct TextFileStreamError
		{
			TextFileStreamErrorType type;
			std::string message;
		};

		class TextFileStream
		{
		public:
			TextFileStream();
			virtual ~TextFileStream();

			static std::string Load(std::string path);
			static void Save(std::string path, std::string data);
		};
	}
}