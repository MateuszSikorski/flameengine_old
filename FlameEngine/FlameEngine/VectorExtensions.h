#pragma once
#include "HeaderFiles.h"

namespace flame
{
	namespace extensions
	{
		std::string ToString(glm::vec2 value);
		std::string ToString(glm::vec3 value);
		std::string ToString(glm::vec4 &value);
	}
}