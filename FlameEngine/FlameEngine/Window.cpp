#include "Window.h"

namespace flame
{
	Window::Window()
	{
		window = nullptr;
		this->title = "Flame Engine";
		this->width = 640;
		this->height = 480;

		window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
		if (window == nullptr)
			throw WindowError("Cannot create window");
	}

	Window::Window(std::string title, int width, int height, bool fullscreen)
	{
		window = nullptr;
		this->title = title;
		this->width = width;
		this->height = height;

		window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
		if (window == nullptr)
			throw WindowError("Cannot create window");
	}

	Window::~Window()
	{
		glfwDestroyWindow(window);
		window = nullptr;
	}

	void Window::CreateRenderContext()
	{
		glfwMakeContextCurrent(window);
	}

	void Window::SwapBuffers()
	{
		glfwSwapBuffers(window);
	}

	bool Window::ShouldClose()
	{
		return glfwWindowShouldClose(window);
	}

	glm::vec2 Window::GetSize()
	{
		return glm::vec2(width, height);
	}
}