#pragma once
#include "HeaderFiles.h"
#include "Material.h"

namespace flame
{
	namespace graphics
	{
		class RenderComponent
		{
		public:
			Material *material;

			RenderComponent();
			virtual ~RenderComponent();

			virtual void Render() = 0;
		};
	}
}
