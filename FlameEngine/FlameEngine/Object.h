#pragma once
#include "RenderComponent.h"
#include "Transform.h"

namespace flame
{
	class Object
	{
	public:
		Transform transform;

		std::vector<graphics::RenderComponent*> renderComponents;

		Object();
		virtual ~Object();

		void DestroyComponents();
	};
}
