#include "Mesh.h"

namespace flame
{
	namespace graphics
	{
		Mesh::Mesh()
		{
			glGenVertexArrays(1, &vao);
		}

		Mesh::~Mesh()
		{
			Delete();
			glDeleteVertexArrays(1, &vao);
		}

		void Mesh::SetVertices(std::vector<glm::vec4> &vertices)
		{
			this->vertices.SetData(vertices);
		}

		void Mesh::Send()
		{
			glBindVertexArray(vao);
			vertices.SendData();
			vertices.SetDataPointer(0, 4, GL_FLOAT, GL_FALSE);
			glEnableVertexAttribArray(0);
		}

		void Mesh::Delete()
		{
			glBindVertexArray(vao);
			glDisableVertexAttribArray(0);
		}

		void Mesh::Bind()
		{
			glBindVertexArray(vao);
		}

		std::vector<glm::vec4> Mesh::GetVertices()
		{
			return vertices.GetData();
		}
	}
}