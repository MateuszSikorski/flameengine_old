#include "ShaderVariableManager.h"

namespace flame
{
	namespace graphics
	{
		ShaderVariableManager::ShaderVariableManager()
		{
			modelMatrix = nullptr;
			viewMatrix = nullptr;
			projectionMatrix = nullptr;
		}

		ShaderVariableManager::~ShaderVariableManager()
		{
			DeleteAllVariables();
		}

		template<class T> void ShaderVariableManager::Create(std::string name)
		{
			T *newVariable = new T();
			ShaderVariable *variable = dynamic_cast<ShaderVariable>newVariable;
			if(variable)
			{
				variable->name = name;
				variables.push_back(variable);
			}
		}

		void ShaderVariableManager::CreateVariables(std::vector <ParsedVariable> &variablesList)
		{
			for (int i = 0; i < variablesList.size(); i++)
			{
				switch (variablesList[i].type)
				{
				case ParsedVariableType::INT:
					variables.push_back(new ShaderVariableInt());
					variables[variables.size() - 1]->name = variablesList[i].name;
					break;
				case ParsedVariableType::FLOAT:
					variables.push_back(new ShaderVariableFloat());
					variables[variables.size() - 1]->name = variablesList[i].name;
					break;
				case ParsedVariableType::VEC2:
					variables.push_back(new ShaderVariableVec2());
					variables[variables.size() - 1]->name = variablesList[i].name;
					break;
				case ParsedVariableType::VEC3:
					variables.push_back(new ShaderVariableVec3());
					variables[variables.size() - 1]->name = variablesList[i].name;
					break;
				case ParsedVariableType::VEC4:
					variables.push_back(new ShaderVariableVec4());
					variables[variables.size() - 1]->name = variablesList[i].name;
					break;
				case ParsedVariableType::MAT4:
					SetMatrices(variablesList[i]);
					break;
				default:
					continue;
				}
			}
		}

		void ShaderVariableManager::Remove(std::string name)
		{
			for (int i = 0; i < variables.size(); i++)
			{
				if (variables[i]->name == name)
				{
					delete variables[i];
					variables.erase(variables.begin() + i);
					break;
				}
			}
		}

		void ShaderVariableManager::DeleteAllVariables()
		{
			for (int i = 0; i < variables.size(); i++)
			{
				delete variables[i];
			}

			variables.clear();

			delete modelMatrix;
			modelMatrix = nullptr;

			delete viewMatrix;
			viewMatrix = nullptr;

			delete projectionMatrix;
			projectionMatrix = nullptr;
		}

		void ShaderVariableManager::GetVariablesLoactions(GLuint programID)
		{
			for(int i = 0; i < variables.size(); i++)
			{
				variables[i]->location = glGetUniformLocation(programID, variables[i]->name.c_str());
			}

			if (modelMatrix != nullptr)
			{
				modelMatrix->location = glGetUniformLocation(programID, modelMatrix->name.c_str());
			}
			if (viewMatrix != nullptr)
			{
				viewMatrix->location = glGetUniformLocation(programID, viewMatrix->name.c_str());
			}
			if (projectionMatrix != nullptr)
			{
				projectionMatrix->location = glGetUniformLocation(programID, projectionMatrix->name.c_str());
			}
		}

		std::vector<std::string> ShaderVariableManager::GetVariableNames()
		{
			std::vector<std::string> names(variables.size());

			for (int i = 0; i < variables.size(); i++)
			{
				names[i] = variables[i]->name;
			}

			return names;
		}

		ParsedVariableType ShaderVariableManager::GetVariableType(std::string name)
		{
			ShaderVariable *variable = GetVariableByName(name);
			if (variable != nullptr)
			{
				return variable->type;
			}

			return ParsedVariableType::UNKNOW_TYPE;
		}

		void ShaderVariableManager::Send()
		{
			for (int i = 0; i < variables.size(); i++)
			{
				variables[i]->Send();
			}
		}

		ShaderVariable* ShaderVariableManager::GetVariableByName(std::string name)
		{
			for (int i = 0; i < variables.size(); i++)
			{
				if (variables[i]->name == name)
				{
					return variables[i];
				}
			}

			return nullptr;
		}

		void ShaderVariableManager::SendModelMatrix(glm::mat4 modelMatrix)
		{
			if (this->modelMatrix != nullptr)
			{
				dynamic_cast<ShaderVariableMat4*>(this->modelMatrix)->value = modelMatrix;
				this->modelMatrix->Send();
			}
		}

		void ShaderVariableManager::SendViewMatrix(glm::mat4 viewMatrix)
		{
			if (this->viewMatrix != nullptr)
			{
				dynamic_cast<ShaderVariableMat4*>(this->viewMatrix)->value = viewMatrix;
				this->viewMatrix->Send();
			}
		}

		void ShaderVariableManager::SendProjectionMatrix(glm::mat4 projectionMatrix)
		{
			if (this->projectionMatrix != nullptr)
			{
				dynamic_cast<ShaderVariableMat4*>(this->projectionMatrix)->value = projectionMatrix;
				this->projectionMatrix->Send();
			}
		}

		void ShaderVariableManager::SetMatrices(ParsedVariable variable)
		{
			if (variable.name == "modelMatrix")
			{
				modelMatrix = new ShaderVariableMat4();
				modelMatrix->name = variable.name;
			}
			else if (variable.name == "viewMatrix")
			{
				viewMatrix = new ShaderVariableMat4();
				viewMatrix->name = variable.name;
			}
			else if (variable.name == "projectionMatrix")
			{
				projectionMatrix = new ShaderVariableMat4();
				projectionMatrix->name = variable.name;
			}
		}
	}
}