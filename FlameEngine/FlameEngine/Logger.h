#pragma once
#include "HeaderFiles.h"
#include <fstream>

namespace flame
{
	namespace extensions
	{
		class Logger
		{
		public:
			Logger();
			virtual ~Logger();

			static void ClearLog();
			static void Log(std::string message);
		};
	}
}