#pragma once
#include "ShaderVariables.h"

namespace flame
{
	namespace graphics
	{
		class ShaderVariableManager
		{
		private:
			std::vector<ShaderVariable*> variables;
			ShaderVariable *modelMatrix;
			ShaderVariable *viewMatrix;
			ShaderVariable *projectionMatrix;

			void SetMatrices(ParsedVariable variable);

		public:
			ShaderVariableManager();
			virtual ~ShaderVariableManager();

			template<class T> void Create(std::string name);

			void CreateVariables(std::vector <ParsedVariable> &variablesList);
			void Remove(std::string name);
			void DeleteAllVariables();
			void Send();
			void GetVariablesLoactions(GLuint programID);
			void SendModelMatrix(glm::mat4 modelMatrix);
			void SendViewMatrix(glm::mat4 viewMatrix);
			void SendProjectionMatrix(glm::mat4 projectionMatrix);

			std::vector<std::string> GetVariableNames();
			ParsedVariableType GetVariableType(std::string name);
			ShaderVariable* GetVariableByName(std::string name);
		};
	}
}