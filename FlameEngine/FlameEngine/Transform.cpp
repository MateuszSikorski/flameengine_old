#include "Transform.h"

namespace flame
{
	TransformSpaceData::TransformSpaceData(glm::vec3 pos, glm::quat rot, glm::vec3 scale)
	{
		position = pos;
		rotation = rot;
		this->scale = scale;
	}

	glm::mat4 TransformSpaceData::GetTranslation()
	{
		glm::mat4 translation(1.0f);
		translation = glm::translate(translation, position);
		translation *= glm::toMat4(rotation);
		translation = glm::scale(translation, scale);

		return translation;
	}

	Transform::Transform()
	{
		global = TransformSpaceData();
		local = TransformSpaceData();
		parent = nullptr;
	}

	Transform::~Transform()
	{
	}

	glm::mat4 Transform::GetModelMatrix()
	{
		return global.GetTranslation();
	}

	void Transform::RefreshChildren()
	{
		int count = children.size();
		for (int i = 0; i < count; i++)
			children[i]->RefreshGlobal();
	}

	void Transform::RefreshGlobal()
	{
		global.position = (parent->global.rotation * (local.position * parent->global.scale)) + parent->global.position;
		global.rotation = local.rotation * parent->global.rotation;
		global.scale = local.scale * parent->global.scale;

		RefreshChildren();
	}

	void Transform::SetGlobalPosition(glm::vec3 pos)
	{
		if (global.position != pos)
		{
			global.position = pos;

			if (parent != nullptr)
				local.position = (global.position - parent->global.position) / parent->global.scale;

			RefreshChildren();
		}
	}

	void Transform::SetGlobalRotation(glm::quat rot)
	{
		if (global.rotation != rot)
		{
			global.rotation = rot;

			if (parent != nullptr)
			{
				//TODO
			}

			RefreshChildren();
		}
	}

	void Transform::SetGlobalEulerAngles(glm::vec3 rot)
	{
		global.rotation = glm::rotate(global.rotation, rot.x, glm::vec3(1, 0, 0));
		global.rotation = glm::rotate(global.rotation, rot.y, glm::vec3(0, 1, 0));
		global.rotation = glm::rotate(global.rotation, rot.z, glm::vec3(0, 0, 1));

		RefreshGlobal();
	}

	void Transform::SetGlobalScale(glm::vec3 s)
	{
		if (global.scale != s)
		{
			global.scale = s;

			if (parent != nullptr)
				local.scale = global.scale / parent->global.scale;

			RefreshChildren();
		}
	}

	void Transform::SetLocalPosition(glm::vec3 pos)
	{
		if (parent != nullptr)
		{
			if (local.position != pos)
			{
				local.position = pos;
				RefreshGlobal();
			}
		}
		else
			SetGlobalPosition(pos);
	}

	void Transform::SetLocalRotation(glm::quat rot)
	{
		if (parent != nullptr)
		{
			if (local.rotation != rot)
			{
				local.rotation = rot;
				RefreshGlobal();
			}
		}
		else
			SetGlobalRotation(rot);
	}

	void Transform::SetLocalEulerAngles(glm::vec3 rot)
	{
		if (parent != nullptr)
		{
			local.rotation = glm::rotate(local.rotation, rot.x, glm::vec3(1, 0, 0));
			local.rotation = glm::rotate(local.rotation, rot.y, glm::vec3(0, 1, 0));
			local.rotation = glm::rotate(local.rotation, rot.z, glm::vec3(0, 0, 1));

			RefreshGlobal();
		}
		else
			SetGlobalEulerAngles(rot);
	}

	void Transform::SetLocalScale(glm::vec3 s)
	{
		if (parent != nullptr)
		{
			if (local.scale != s)
			{
				local.scale = s;
				RefreshGlobal();
			}
		}
		else
			SetGlobalScale(s);
	}

	void Transform::RemoveParent()
	{
		parent->RemoveChild(this);
		parent = nullptr;
	}

	void Transform::AddChild(Transform *child)
	{
		children.push_back(child);
		child->parent = this;
	}

	void Transform::RemoveChild(int num)
	{
		if (num < children.size())
		{
			children[num]->RemoveParent();
			children.erase(children.begin() + num);
		}
	}

	void Transform::RemoveChild(Transform *child)
	{
		int count = children.size();
		for (int i = 0; i < count; i++)
		{
			if (child == children[i])
			{
				children.erase(children.begin() + i);
				break;
			}
		}
	}

	Transform *Transform::GetChild(int num)
	{
		return children[num];
	}

	glm::vec3 Transform::GetGlobalEulerAngles()
	{
		return glm::vec3();
	}
}