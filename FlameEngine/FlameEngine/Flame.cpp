#include "Flame.h"
#include <iostream>

namespace flame
{
	Flame::Flame()
	{
	}

	Flame::~Flame()
	{
	}

	void Flame::Init(int majorVersion, int minorVersion)
	{
		if (!glfwInit())
			throw FlameError("Cannot initialize GLFW 3");
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, majorVersion);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minorVersion);
	}

	void Flame::AttachWindow(Window *window)
	{
		this->window = window;
		this->window->CreateRenderContext();
		glfwSwapInterval(1);

		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK)
			throw FlameError("Cannot initialize GLEW");
	}

	void Flame::Work()
	{
		while (!window->ShouldClose())
		{
			glfwPollEvents();
			
			masterRenderer.Render(objects);
			window->SwapBuffers();
		}
	}

	void Flame::Destroy()
	{
		for (auto *object : objects)
		{
			delete object;
		}
		objects.clear();

		delete window;
		glfwTerminate();
	}

	void Flame::AddObject(Object *newObject)
	{
		objects.push_back(newObject);
		masterRenderer.AddCamera(newObject, window->GetSize());
	}

	glm::vec2 Flame::GetViewport()
	{
		return window->GetSize();
	}
}