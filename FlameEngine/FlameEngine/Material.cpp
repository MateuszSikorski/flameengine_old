#include "Material.h"

namespace flame
{
	namespace graphics
	{
		Material::Material()
		{
		}

		Material::~Material()
		{
			Destroy();
		}

		void Material::AttachShader(ShaderProgram *newShader)
		{
			shader = newShader;
			InitVariables();
		}

		void Material::InitVariables()
		{
			variables.clear();
			if (shader == nullptr)
			{
				return;
			}

			std::vector<std::string> names = shader->variableManager.GetVariableNames();
			for (auto name : names)
			{
				MaterialVariable *newMaterialVariable = nullptr;
				ShaderVariable *shaderVariable = shader->variableManager.GetVariableByName(name);
				switch (shaderVariable->type)
				{
				case ParsedVariableType::INT:
					newMaterialVariable = new MaterialVariableInt();
					break;
				case ParsedVariableType::FLOAT:
					newMaterialVariable = new MaterialVariableFloat();
					break;
				case ParsedVariableType::VEC2:
					newMaterialVariable = new MaterialVariableVec2();
					break;
				case ParsedVariableType::VEC3:
					newMaterialVariable = new MaterialVariableVec3();
					break;
				case ParsedVariableType::VEC4:
					newMaterialVariable = new MaterialVariableVec4();
					break;
				default:
					break;
				}

				if (newMaterialVariable != nullptr)
				{
					newMaterialVariable->SetVariable(shaderVariable);
					variables.push_back(newMaterialVariable);
				}
			}
		}

		void Material::SendValues()
		{
			for (auto *variable : variables)
			{
				variable->SendValue();
			}
		}

		void Material::Destroy()
		{
			for (auto *variable : variables)
			{
				delete variable;
			}

			variables.clear();
		}

		void Material::SetInt(std::string name, int value)
		{
			MaterialVariableInt *intVariable = dynamic_cast<MaterialVariableInt*>(GetVariableByName(name));
			if (intVariable != nullptr)
			{
				intVariable->value = value;
			}
		}

		void Material::SetFloat(std::string name, float value)
		{
			MaterialVariableFloat *floatVariable = dynamic_cast<MaterialVariableFloat*>(GetVariableByName(name));
			if (floatVariable != nullptr)
			{
				floatVariable->value = value;
			}
		}

		void Material::SetVec2(std::string name, glm::vec2 value)
		{
			MaterialVariableVec2 *vec2Variable = dynamic_cast<MaterialVariableVec2*>(GetVariableByName(name));
			if (vec2Variable != nullptr)
			{
				vec2Variable->value = value;
			}
		}

		void Material::SetVec3(std::string name, glm::vec3 value)
		{
			MaterialVariableVec3 *vec3Variable = dynamic_cast<MaterialVariableVec3*>(GetVariableByName(name));
			if (vec3Variable != nullptr)
			{
				vec3Variable->value = value;
			}
		}

		void Material::SetVec4(std::string name, glm::vec4 &value)
		{
			MaterialVariableVec4 *vec4Variable = dynamic_cast<MaterialVariableVec4*>(GetVariableByName(name));
			if (vec4Variable != nullptr)
			{
				vec4Variable->value = value;
			}
		}

		MaterialVariable *Material::GetVariableByName(std::string name)
		{
			for (auto *variable : variables)
			{
				if (variable->GetName() == name)
				{
					return variable;
				}
			}

			return nullptr;
		}

		ShaderProgram *Material::GetShader()
		{
			return shader;
		}
	}
}