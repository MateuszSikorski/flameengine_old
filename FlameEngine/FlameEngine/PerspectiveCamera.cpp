#include "PerspectiveCamera.h"

namespace flame
{
	PerspectiveCamera::PerspectiveCamera()
	{
		nearPlane = 0.1f;
		farPlane = 1000.f;
		aspect = 16.f / 10.f;
		fov = 45.f;
		projectionMatrix = glm::perspective(fov, aspect, nearPlane, farPlane);
	}

	PerspectiveCamera::~PerspectiveCamera()
	{
	}

	void PerspectiveCamera::SetViewport(glm::ivec2 viewport)
	{
		viewportWidth = viewport.x;
		viewportHeight = viewport.y;
		SetAspect();
	}

	void PerspectiveCamera::SetAspect()
	{
		aspect = (float)viewportWidth / (float)viewportHeight;
		projectionMatrix = glm::perspective(fov, aspect, nearPlane, farPlane);
	}

	void PerspectiveCamera::SetFOV(float value)
	{
		fov = value;
		projectionMatrix = glm::perspective(fov, aspect, nearPlane, farPlane);
	}

	void PerspectiveCamera::SetNearPlane(float value)
	{
		nearPlane = value;
		projectionMatrix = glm::perspective(fov, aspect, nearPlane, farPlane);
	}

	void PerspectiveCamera::SetFarPlane(float value)
	{
		farPlane = value;
		projectionMatrix = glm::perspective(fov, aspect, nearPlane, farPlane);
	}

	void PerspectiveCamera::RefreshSceneForRendering()
	{
		Camera::RefreshSceneForRendering();
	}

	glm::mat4 PerspectiveCamera::GetViewMatrix()
	{
		return Camera::GetViewMatrix();
	}

	glm::mat4 PerspectiveCamera::GetProjectionMatrix()
	{
		return Camera::GetProjectionMatrix();
	}
}