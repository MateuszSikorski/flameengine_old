#include "TextFileStream.h"

namespace flame
{
	namespace extensions
	{
		TextFileStream::TextFileStream()
		{
		}

		TextFileStream::~TextFileStream()
		{
		}

		std::string TextFileStream::Load(std::string path)
		{
			std::ifstream file;
			file.open(path);

			if (!file.is_open())
			{
				file.close();
				TextFileStreamError error;
				error.type = TextFileStreamErrorType::NO_LOAD_FILE;
				error.message = "Cannot open file to load: " + path;
				throw error;
			}

			std::string fileData;
			std::string line;
			while (std::getline(file, line))
			{
				fileData += line + '\n';
			}

			file.close();

			return fileData;
		}

		void TextFileStream::Save(std::string path, std::string data)
		{
			std::ofstream file;
			file.open(path);

			if (!file.is_open())
			{
				file.close();
				TextFileStreamError error;
				error.type = TextFileStreamErrorType::NO_SAVE_FILE;
				error.message = "Cannot open file to save: " + path;
				throw error;
			}

			file << data;
			file.close();
		}
	}
}