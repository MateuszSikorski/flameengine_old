#pragma once
#include "HeaderFiles.h"

namespace flame
{
	struct WindowError
	{
		std::string message;

		WindowError()
		{
		}

		WindowError(std::string message)
		{
			this->message = message;
		}
	};

	class Window
	{
	private:
		GLFWwindow *window;
		int width, height;
		std::string title;

	public:
		Window();
		Window(std::string title, int width, int height, bool fullscreen);
		virtual ~Window();

		void CreateRenderContext();
		void SwapBuffers();

		bool ShouldClose();

		glm::vec2 GetSize();
	};
}