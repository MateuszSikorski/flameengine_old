#pragma once

#include "HeaderFiles.h"
#include "ParsedShader.h"
#include <regex>

namespace flame
{
	namespace graphics
	{
		struct ShaderParserError
		{
			std::string message;
		};

		class ShaderParser
		{
		private:
			std::vector <std::string> errorLog;
			std::string *dataToParse;
			std::string version;
			ParsedShader parsedShader;

			std::string CreateErrorLogMessage();

			void WrongElements(std::string wrongElementString, std::string block, std::string data);
			void ParseVersion();
			void ParseShaderCode(std::string type, std::string &destination);
			void ParseVertexShaderCode();
			void ParseFragmentShaderCode();
			void ParseVariables();

		public:
			ShaderParser();
			virtual ~ShaderParser();

			ParsedShader Parse(std::string data);
		};
	}
}

