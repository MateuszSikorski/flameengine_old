#include "GraphicsBuffer.h"

namespace flame
{
	namespace graphics
	{
		template<typename T> GraphicsBuffer<T>::GraphicsBuffer()
		{
			this->type = GL_ARRAY_BUFFER;
			this->usageType = GL_STATIC_DRAW;
			glGenBuffers(1, &vbo);
		}

		template<typename T> GraphicsBuffer<T>::GraphicsBuffer(GLenum type = GL_ARRAY_BUFFER, GLenum usageType = GL_STATIC_DRAW)
		{
			this->type = type;
			this->usageType = usageType;
			glGenBuffers(1, &vbo);
		}

		template<typename T> GraphicsBuffer<T>::~GraphicsBuffer()
		{
			glDeleteBuffers(1, &vbo);
		}

		template<typename T> void GraphicsBuffer<T>::SetData(std::vector<T> &newData)
		{
			data.clear();
			data.resize(newData.size());
			for (int i = 0; i < newData.size(); i++)
			{
				data[i] = newData[i];
			}
		}

		template<typename T> void GraphicsBuffer<T>::SetDataPointer(int index, int size, GLenum type, GLboolean normalize)
		{
			glVertexAttribPointer(index, size, type, normalize, 0, 0);
		}

		template<typename T> void GraphicsBuffer<T>::SendData()
		{
			glBindBuffer(type, vbo);
			glBufferData(type, sizeof(T) * data.size(), &data[0], usageType);
		}

		template<typename T> std::vector<T> GraphicsBuffer<T>::GetData()
		{
			return data;
		}
	}
}