#pragma once
#include "ShaderVariableManager.h"

namespace flame
{
	namespace graphics
	{
		class ShaderProgram
		{
		public:
			GLuint programID;
			ShaderVariableManager variableManager;

			ShaderProgram();
			virtual ~ShaderProgram();

			void Use();
			void InitVariables();
		};
	}
}