#pragma once
#include "Object.h"

namespace flame
{
	enum class CameraClearFlag
	{
		COLOR_DEPTH,
		DEPTH,
		NOTHING
	};

	class Camera : public Object
	{
	protected:
		glm::mat4 viewMatrix;
		glm::mat4 projectionMatrix;
		float nearPlane;
		float farPlane;

	public:
		CameraClearFlag clearFlag;
		glm::vec4 clearColor;
		float viewportWidth;
		float viewportHeight;

		Camera();
		virtual ~Camera();

		virtual void SetViewport(glm::ivec2 viewport);
		virtual void SetNearPlane(float value) = 0;
		virtual void SetFarPlane(float value) = 0;
		virtual void RefreshSceneForRendering();
		virtual glm::mat4 GetViewMatrix();
		virtual glm::mat4 GetProjectionMatrix();

		float GetNearPlane() { return nearPlane; }
		float GetFarPlane() { return farPlane; }
	};
}