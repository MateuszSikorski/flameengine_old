#include "GPUdata.h"

namespace flame
{
	GPUdata::GPUdata()
	{
	}

	GPUdata::~GPUdata()
	{
	}

	void GPUdata::GetDataFromDevice()
	{
		vendor = (const char*)glGetString(GL_VENDOR);
		renderer = (const char*)glGetString(GL_RENDERER);
		openGLVersion = (const char*)glGetString(GL_VERSION);
		glslVersion = (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION);
	}
}