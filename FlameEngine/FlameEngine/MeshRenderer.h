#pragma once
#include "RenderComponent.h"
#include "Mesh.h"

namespace flame
{
	namespace graphics
	{
		class MeshRenderer : public RenderComponent
		{
		protected:
			Mesh *mesh;

		public:
			MeshRenderer();
			virtual ~MeshRenderer();

			void AttachMesh(Mesh *newMesh);

			virtual void Render() override;
		};
	}
}