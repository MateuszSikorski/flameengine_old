#pragma once
#include "HeaderFiles.h"
#include "ParsedVariable.h"

namespace flame
{
	namespace graphics
	{
		struct ShaderVariable
		{
			std::string name;
			GLuint location;
			ParsedVariableType type;

			virtual void Send() = 0;
		};

		struct ShaderVariableInt : public ShaderVariable
		{
			int value;

			ShaderVariableInt()
			{
				type = ParsedVariableType::INT;
			}

			virtual void Send() override
			{
				glUniform1i(location, value);
			}
		};

		struct ShaderVariableFloat : public ShaderVariable
		{
			float value;

			ShaderVariableFloat()
			{
				type = ParsedVariableType::FLOAT;
			}

			virtual void Send() override
			{
				glUniform1f(location, value);
			}
		};

		struct ShaderVariableVec2 : public ShaderVariable
		{
			glm::vec2 value;

			ShaderVariableVec2()
			{
				type = ParsedVariableType::VEC2;
			}

			virtual void Send() override
			{
				glUniform2fv(location, 1, &value[0]);
			}
		};

		struct ShaderVariableVec3 : public ShaderVariable
		{
			glm::vec3 value;

			ShaderVariableVec3()
			{
				type = ParsedVariableType::VEC3;
			}

			virtual void Send() override
			{
				glUniform3fv(location, 1, &value[0]);
			}
		};

		struct ShaderVariableVec4 : public ShaderVariable
		{
			glm::vec4 value;

			ShaderVariableVec4()
			{
				type = ParsedVariableType::VEC4;
			}

			virtual void Send() override
			{
				glUniform4fv(location, 1, &value[0]);
			}
		};

		struct ShaderVariableMat4 : public ShaderVariable
		{
			glm::mat4 value;

			ShaderVariableMat4()
			{
				type = ParsedVariableType::MAT4;
			}

			virtual void Send() override
			{
				glUniformMatrix4fv(location, 1, GL_FALSE, &value[0][0]);
			}
		};
	}
}