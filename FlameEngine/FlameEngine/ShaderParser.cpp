#include "ShaderParser.h"
#include "Extensions.h"

using namespace std;

namespace flame
{
	namespace graphics
	{
		ShaderParser::ShaderParser()
		{
		}

		ShaderParser::~ShaderParser()
		{
		}

		string ShaderParser::CreateErrorLogMessage()
		{
			string errorMessage;
			for (int i = 0; i < errorLog.size(); i++)
			{
				errorMessage += errorLog[i] + "\n";
			}

			return errorMessage;
		}

		void ShaderParser::ParseVersion()
		{
			regex pattern("version *([0-9]*) *(core|compatibility)*");
			smatch result;
			if (regex_search(*dataToParse, result, pattern))
			{
				version = "#version ";
				if (result[1].matched && result[2].matched)
				{
					version += result[1].str();
					version += " ";
					version += result[2].str();
					version += "\n";
				}
				else
					errorLog.push_back(string("Incomplete shader version"));
			}
			else
				errorLog.push_back(string("No shader version"));
		}

		void ShaderParser::ParseShaderCode(string type, string &destination)
		{
			string blockStart("<" + type + ">");
			string blockEnd("</" + type + ">");
			int start = dataToParse->find(blockStart);
			int end = dataToParse->find(blockEnd);
			if (start < 0)
				errorLog.push_back(string("No start of " + type + " block"));
			if (end < 0)
				errorLog.push_back(string("No end of " + type + " block"));
			if (start > end)
				errorLog.push_back(string("Start of " + type + " block is in wrong place"));

			if (start >= 0 && end >= 0 && end > start)
			{
				start += blockStart.length();
				destination = dataToParse->substr(start, end - start);
			}
		}

		void ShaderParser::WrongElements(string wrongElementString, string block, string data)
		{
			string wronElementString("Wrong element in " + block + " block: ");
			int wrongElement = data.find(wrongElementString);
			if (wrongElement >= 0)
				errorLog.push_back(wronElementString + wrongElementString);
		}

		void ShaderParser::ParseVertexShaderCode()
		{
			string blockName("vertex");
			ParseShaderCode(blockName, parsedShader.vertexShaderCode);
			if (parsedShader.vertexShaderCode.empty())
				return;

			parsedShader.vertexShaderCode = version + parsedShader.vertexShaderCode;

			WrongElements("<fragment>", blockName, parsedShader.vertexShaderCode);
			WrongElements("</fragment>", blockName, parsedShader.vertexShaderCode);
		}

		void ShaderParser::ParseFragmentShaderCode()
		{
			string blockName("fragment");
			ParseShaderCode(blockName, parsedShader.fragmentShaderCode);
			if (parsedShader.fragmentShaderCode.empty())
				return;

			parsedShader.fragmentShaderCode = version + parsedShader.fragmentShaderCode;

			WrongElements("<vertex>", blockName, parsedShader.fragmentShaderCode);
			WrongElements("</vertex>", blockName, parsedShader.fragmentShaderCode);
		}

		void ShaderParser::ParseVariables()
		{
			vector <string> splitString;
			extensions::StringSplit(&parsedShader.vertexShaderCode, '\n', &splitString);
			extensions::StringSplit(&parsedShader.fragmentShaderCode, '\n', &splitString);
			for (int i = 0; i < splitString.size(); i++)
			{
				regex pattern("uniform\\s*(\\w*)\\s*(\\w*)\\s*;");
				smatch result;
				if (regex_search(splitString[i], result, pattern))
				{
					ParsedVariable variable;
					variable.type = ParseType(result[1].str());
					variable.name = result[2];
					parsedShader.variables.push_back(variable);
				}
			}
		}

		ParsedShader ShaderParser::Parse(string data)
		{
			dataToParse = &data;
			ParseVersion();
			ParseVertexShaderCode();
			ParseFragmentShaderCode();
			ParseVariables();

			if (errorLog.size() > 0)
			{
				ShaderParserError error;
				error.message = CreateErrorLogMessage();
				throw error;
			}

			return parsedShader;
		}
	}
}
