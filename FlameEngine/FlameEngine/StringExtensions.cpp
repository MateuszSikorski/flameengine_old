#include "StringExtensions.h"

namespace flame
{
	namespace extensions
	{
		void StringSplit(std::string *data, char dispacher, std::vector <std::string> *out)
		{
			std::string tempString;
			std::stringstream stream(*data);
			while (getline(stream, tempString, dispacher))
				out->push_back(tempString);
		}
	}
}