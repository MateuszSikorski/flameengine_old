#include "ShaderManager.h"

namespace flame
{
	namespace graphics
	{
		ShaderManager::ShaderManager()
		{
		}

		ShaderManager::~ShaderManager()
		{
			DeleteShaders();
		}

		ShaderProgram *ShaderManager::LoadShader(std::string path)
		{
			ShaderProgram *program = nullptr;
			try
			{
				ShaderParser parser;
				flame::graphics::ShaderCompiler compiler;
				flame::graphics::ParsedShader parsedShader = parser.Parse(flame::extensions::TextFileStream::Load(path));
				program = compiler.Compile(&parsedShader);
				program->variableManager.CreateVariables(parsedShader.variables);
				program->InitVariables();
			}
			catch (flame::extensions::TextFileStreamError error)
			{
				extensions::Logger::Log(error.message);
			}
			catch (flame::graphics::ShaderParserError error)
			{
				extensions::Logger::Log(error.message);
			}
			catch (flame::graphics::ShaderCompilerError error)
			{
				extensions::Logger::Log(error.message);
			}

			return program;
		}

		ShaderProgram *ShaderManager::GetShader(std::string path)
		{
			if (shaders.find(path) != shaders.end())
			{
				return shaders[path];
			}
			else
			{
				ShaderProgram *program = LoadShader(path);
				if (program != nullptr)
				{
					shaders.insert(std::pair<std::string, ShaderProgram*>(path, program));
				}

				return program;
			}
		}

		void ShaderManager::DeleteShaders()
		{
			for (auto &x : shaders)
			{
				delete x.second;
			}

			shaders.clear();
		}
	}
}