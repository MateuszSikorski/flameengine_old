#pragma once
#include "ShaderProgram.h"
#include "ShaderParser.h"
#include "ShaderCompiler.h"
#include "Extensions.h"

namespace flame
{
	namespace graphics
	{
		class ShaderManager
		{
		private:
			std::map <std::string, ShaderProgram*> shaders;
			ShaderProgram * LoadShader(std::string path);

		public:
			ShaderManager();
			virtual ~ShaderManager();

			ShaderProgram *GetShader(std::string path);

			void DeleteShaders();
		};
	}
}