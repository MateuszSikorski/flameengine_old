#pragma once
#include "ShaderParser.h"
#include "ShaderProgram.h"

namespace flame
{
	namespace graphics
	{
		struct ShaderCompilerError
		{
			std::string message;
		};

		class ShaderCompiler
		{
		private:
			std::vector <std::string> errorLog;
			GLuint vertexShader;
			GLuint fragmentShader;

			std::string CreateErrorLogMessage();
			void CompileShader(std::string data, GLuint *shader, GLenum type, std::string logTag);
			void DeleteShaders();
			void LinkProgram(ShaderProgram *program);

		public:
			ShaderCompiler();
			virtual ~ShaderCompiler();

			ShaderProgram* Compile(ParsedShader *parsedShader);
		};
	}
}