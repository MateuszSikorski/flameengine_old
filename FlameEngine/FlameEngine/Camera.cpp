#include "Camera.h"

namespace flame
{
	Camera::Camera()
	{
		clearColor = glm::vec4(0,0,0,0);
		clearFlag = CameraClearFlag::COLOR_DEPTH;
		viewportHeight = viewportWidth = 0.f;
	}

	Camera::~Camera()
	{
	}

	void Camera::SetViewport(glm::ivec2 viewport)
	{
		viewportWidth = viewport.x;
		viewportHeight = viewport.y;
	}

	void Camera::RefreshSceneForRendering()
	{
		glViewport(0, 0, viewportWidth, viewportHeight);
		switch (clearFlag)
		{
		case CameraClearFlag::COLOR_DEPTH:
			glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			break;
		case CameraClearFlag::DEPTH:
		{
			GLfloat depth[] = { 1,1,1,1 };
			glClearBufferfv(GL_DEPTH, 0, depth);
		}
		case CameraClearFlag::NOTHING:
			break;
		}
	}

	glm::mat4 Camera::GetViewMatrix()
	{
		viewMatrix = glm::translate(glm::mat4(1.f), -transform.GetGlobalPosition());
		viewMatrix *= glm::mat4_cast(transform.GetGlobalRotation());

		return viewMatrix;
	}

	glm::mat4 Camera::GetProjectionMatrix()
	{
		return projectionMatrix;
	}
}