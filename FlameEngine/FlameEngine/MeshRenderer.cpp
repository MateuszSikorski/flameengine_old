#include "MeshRenderer.h"

namespace flame
{
	namespace graphics
	{
		MeshRenderer::MeshRenderer()
		{
		}

		MeshRenderer::~MeshRenderer()
		{
		}

		void MeshRenderer::AttachMesh(Mesh *newMesh)
		{
			mesh = newMesh;
		}

		void MeshRenderer::Render()
		{
			if (mesh != nullptr)
			{
				mesh->Bind();
				glDrawArrays(GL_TRIANGLES, 0, mesh->GetVertices().size());
			}
		}
	}
}