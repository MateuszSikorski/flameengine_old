#pragma once
#include "Window.h"
#include "Object.h"
#include "MasterRenderer.h"

namespace flame
{
	struct FlameError
	{
		std::string message;

		FlameError()
		{
		}

		FlameError(std::string message)
		{
			this->message = message;
		}
	};

	class Flame
	{
	private:
		Window *window;
		graphics::MasterRenderer masterRenderer;
		std::vector<Object*> objects;

	public:
		Flame();
		virtual ~Flame();

		void Init(int majorVersion, int minorVersion);
		void AttachWindow(Window *window);
		void Work();
		void Destroy();
		void AddObject(Object *newObject);

		glm::vec2 GetViewport();
	};
}

